#WarnView

##示例代码

```java 
WarnView warn2 = (WarnView) findViewById(R.id.warn2);

warn2.setNums(11);
warn2.setTextSize(18);
warn2.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
         WarnView warn2 = (WarnView) v;
         warn2.shake(false, -10, 11, 50);//横向振动
    }
});
```

```xml
<com.simen.warnview.WarnView
        android:id="@+id/warn1"
        xmlns:warn="http://schemas.android.com/apk/res/com.simen.sample"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:src="@mipmap/ic_launcher"
        android:scaleType="center"
        warn:warn_gravity="right|bottom"
        warn:warn_paddingBottom="3dp"
        warn:warn_paddingRight="3dp"
        warn:warn_src="@mipmap/personal_newmessage"/>
```

效果图:

![screenshot](screencapture1.png)

![screenshot](screencapture2.gif)

##gradle
```groovy
compile 'com.simen.warnview:WarnView:1.5.0'
```
