package com.simen.sample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.simen.warnview.WarnView;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WarnView warn2 = (WarnView) findViewById(R.id.warn2);
        WarnView warn3 = (WarnView) findViewById(R.id.warn3);
        WarnView warn4 = (WarnView) findViewById(R.id.warn4);
        WarnView warn5 = (WarnView) findViewById(R.id.warn5);

        warn2.setNums(11);
        warn2.setTextSize(18);
        warn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WarnView warn2 = (WarnView) v;
                warn2.shake(false, -10, 11, 50);//横向振动
            }
        });

        warn3.setWarnImage(R.mipmap.menu_messageico);
        warn3.setNums(81);
        warn3.setTextSize(18);
        warn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WarnView warn3 = (WarnView) v;
                warn3.shake(true, 50, 11, 50);//横向振动
            }
        });

        warn4.setWarnImage(R.mipmap.message_detail_icon);
        warn4.setNums(81);
        warn4.setTextSize(22);
        warn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WarnView warn3 = (WarnView) v;
                warn3.shake(true, 50, 11, 50);//横向振动
            }
        });

        warn5.setWarnImage(R.mipmap.message_detail_icon);
        warn5.setNums(81);
        warn5.setTextSize(22);
        warn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WarnView warn3 = (WarnView) v;
                warn3.shake(true, 50, 11, 50);//横向振动
            }
        });

    }
}
